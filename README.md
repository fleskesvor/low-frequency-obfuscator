Low Frequency Obfuscator
========================

A script to replace words below a certain frequency treshold in the language with junk to simulate what it feels like
to read a text in a language in which you only know a few words.

Word frequency list
-------------------

The frequency list is based on the
[word frequency list from NoWaC](http://www.hf.uio.no/iln/om/organisasjon/tekstlab/tjenester/nowac-frequency.html)
and has been pre-processed using [awk](https://www.gnu.org/software/gawk/):

```
awk 'NF==4' nowac-1.1.words.freq |  # remove entries with more than one word (eg. quotes)
awk '$NF!~/^symb/' |                # remove entries tagged as symbols
awk '$NF!~/^ukjent/' |              # remove entries tagged as unknown (eg. timestamps)
awk '$NF!~/^subst_<dato>/' |        # remove entries tagged as dates
awk '!($NF~/^det/ && $3~/[0-9]/)' | # remove entries recognized as numbers
cat > nowac-1.1.words.freq.filtered
    
go run internal/cmd/parser/parse.go > nowac-10000.csv

go generate internal/storage/storage.go
```

Checksums
---------

| Filename                      | md5                              |
| :---                          | :---                             |
| nowac-1.1.words.freq          | ce5a1a15be2325f9297d5b93bc07db41 |
| nowac-1.1.words.freq.filtered | 4c52e90f3c0921817f0d1d289103ff53 |
| nowac-10000.csv               | ?                                |

TODOs
-----

* Make generation of nowac-10000.csv deterministic
* Keep "er" and "har" unchanged
