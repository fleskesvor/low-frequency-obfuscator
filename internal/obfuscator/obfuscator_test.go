package obfuscator

import (
	"gitlab.com/fleskesvor/lfo"
	"gitlab.com/fleskesvor/lfo/internal/storage"
	"testing"
)

func TestObfuscator_ObfuscateText(t *testing.T) {
	o := NewObfuscator(makeTestStore())
	wants := map[string]string{
		"Hei på deg, Verden!": "Jio qa fih, Verden!", // "Verden" proper noun -> not obfuscated
		"Mønen er kul!": "Nåpen is lum!", // Proper noun "Mønen" first in sentence -> obfuscated
		"Ingen grunn til å være sulten når det er flesk i huset.":
			"Ophen hsunn vom a være tumven pas fiv is gmisk o jutet.", // "være" within threshold - not obfuscated
	}

	for text, want := range wants {
		got, _ := o.ObfuscateText(text, 10)
		if got != want {
			t.Fatalf(`ObfuscateWord("%s") = %q, want "%s"`, text, got, want)
		}
	}
}

func TestObfuscator_ObfuscateWord(t *testing.T) {
	o := NewObfuscator(makeTestStore())
	wants := map[string]string{
		"være": "være", // within threshold -> not obfuscated
		"pølse": "qåmse", // not in list -> obfuscated
		"flesk": "gmisk", // outside threshold > obfuscated
	}

	for word, want := range wants {
		got := o.ObfuscateWord(word, 10)
		if got != want {
			t.Fatalf(`ObfuscateWord("%s") = %q, want "%s"`, word, got, want)
		}
	}
}

func makeTestStore() *storage.Store {
	return &storage.Store{Storage: map[string]lfo.Entry{
		"være": {Word: "være", Rank: 1},
		"flesk": {Word: "flesk", Rank: 11},
	}}
}
