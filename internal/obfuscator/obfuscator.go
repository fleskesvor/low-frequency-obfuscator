package obfuscator

import (
	"gitlab.com/fleskesvor/lfo"
	"gitlab.com/fleskesvor/lfo/internal/parser"
	"gitlab.com/fleskesvor/lfo/internal/storage"
	"strings"
	"unicode"
)

type Obfuscator struct {
	store     *storage.Store
}

func NewObfuscator(store *storage.Store) *Obfuscator {
	return &Obfuscator{store: store}
}

func (o *Obfuscator) ObfuscateText(text string, threshold int) (string, []lfo.Token) {
	tokens := parser.GetTokens(text)
	var replacement = make([]string, len(tokens))

	for index, token := range tokens {
		word := token.Word
		if !token.Name && !token.Number {
			word = o.ObfuscateWord(token.Word, threshold)
		}
		replacement[index] = word + token.Punctuation
	}
	return strings.Join(replacement, ""), tokens
}

func (o *Obfuscator) ObfuscateWord(word string, threshold int) string {
	lookup, found := o.store.Get(strings.ToLower(word))
	if found && lookup.Rank < threshold {
		return word
	}
	return obfuscate(word)
}


func obfuscate(word string) string {
	length := len([]rune(word))
	index := 0 // first argument in range gives byte position, not index
	var obfuscated = make([]rune, length)

	for _, original := range word {
		isUpper := unicode.IsUpper(original)
		original = unicode.ToLower(original)

		if length >= 5 && index >= length - 2 {
			obfuscated[index] = original
		} else if replacement, found := vovelReplacements[original]; found {
			obfuscated[index] = replacement
		} else if replacement, found = consonantReplacements[original]; found {
			obfuscated[index] = replacement
		} else {
			obfuscated[index] = original
		}
		if isUpper {
			obfuscated[index] = unicode.ToUpper(obfuscated[index])
		}
		index++
	}
	return string(obfuscated)
}

var vovelReplacements = map[rune]rune{
	'a': 'e',
	'e': 'i',
	'i': 'o',
	'o': 'u',
	'y': 'æ',
	'æ': 'ø',
	'ø': 'å',
	'å': 'a',
}

var consonantReplacements = map[rune]rune{
	'b': 'c',
	'c': 'd',
	'd': 'f',
	'f': 'g',
	'g': 'h',
	'h': 'j',
	'j': 'k',
	'k': 'l',
	'l': 'm',
	'm': 'n',
	'n': 'p',
	'p': 'q',
	'q': 'r',
	'r': 's',
	's': 't',
	't': 'v',
	'v': 'w',
	'w': 'x',
	'x': 'z',
	'z': 'b',
}
