//go:generate go run generator.go

package storage

import "gitlab.com/fleskesvor/lfo"

type Store struct {
	Storage map[string]lfo.Entry
}

var StoreSize = 55_000
var store = &Store{Storage: make(map[string]lfo.Entry, StoreSize)}

func NewStore() *Store {
	return store
}

func (s *Store) Add(word string, entry lfo.Entry) {
	s.Storage[word] = entry
}

func (s *Store) Get(word string) (lfo.Entry, bool) {
	entry, found := s.Storage[word]
	return entry, found
}
