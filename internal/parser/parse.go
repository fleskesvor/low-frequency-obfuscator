package parser

import (
	"fmt"
	"gitlab.com/fleskesvor/lfo"
	"regexp"
	"strconv"
	"unicode"
)

var separators = `\s-.,!?:;"`
var fieldPattern = regexp.MustCompile(fmt.Sprintf("[^%[1]s]+[%[1]s]*", separators))
var punctPattern = regexp.MustCompile(fmt.Sprintf("[%s]", separators))
var endRunes = map[rune]string{
	33: "!",
	46: ".",
	63: "?",
}

func GetTokens(text string) []lfo.Token {
	fields := fieldPattern.FindAllString(text, -1)
	tokens := make([]lfo.Token, len(fields))

	sentenceStart := true

	for index, field := range fields {
		word, punctuation := stripPunctuation(field)
		_, numErr := strconv.ParseFloat(word, 64)

		tokens[index] = lfo.Token{
			Word:          word,
			Punctuation:   punctuation,
			Number:        numErr == nil,
			Name:          !sentenceStart && unicode.IsUpper([]rune(word)[0]),
		}
		if punctuation != "" {
			_, sentenceStart = endRunes[[]rune(punctuation)[0]]
		}
	}
	return tokens
}

func stripPunctuation(word string) (string, string) {
	indexes := punctPattern.FindStringIndex(word)
	if len(indexes) < 1 {
		return word, ""
	}
	return word[0: indexes[0]], word[indexes[0]:]
}
