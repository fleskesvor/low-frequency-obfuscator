package parser

import (
	"fmt"
	"regexp"
	"testing"
)

func TestGetTokens(t *testing.T) {
	tokens := GetTokens(getText())

	fmt.Printf("%#v\n", tokens)

	original := "pølse, kåre"
	reg := regexp.MustCompile(`[\s-.,!?:;]`)
	indexes := reg.FindStringIndex(original)
//	fmt.Printf("%#v", indexes)
	word := original[0: indexes[0]]
	punctuation := original[indexes[0]:]
//	parts := reg.Split("asdf, ", 3)
//	fmt.Printf("%#v\n", parts)
	fmt.Println(word)
	fmt.Printf("'%s'", punctuation)
}

func getText() string {
	return `
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
`
}
