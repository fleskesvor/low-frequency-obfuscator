package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

type Stem struct {
	Stem string
	Words []Word
	Total int
}

type Word struct {
	Word string
	Count int
}

type stems []Stem

func (s stems) Len() int { return len(s) }
func (s stems) Less(i, j int) bool { return s[i].Total < s[j].Total }
func (s stems) Swap(i, j int) { s[i], s[j] = s[j], s[i] }

func sorted(sm map[string]*Stem) []Stem {
	sorted := make(stems, 0, len(sm))
	for _, value := range sm {
		sorted = append(sorted, *value)
	}
	sort.Sort(sort.Reverse(sorted))
	return sorted
}

func main() {
	file, err := os.Open("nowac-1.1.words.freq.filtered")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var seen = make(map[string]bool)
	var stems = make(map[string]*Stem)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fields := strings.Fields(scanner.Text())
		count, _ := strconv.Atoi(fields[0])
		word := fields[1]
		stem := fields[2]

		if _, found := seen[word]; found {
			continue
		}
		seen[word] = true

		if _, found := stems[stem]; !found {
			stems[stem] = &Stem{
				Stem: stem,
			}
		}

		stems[stem].Words = append(stems[stem].Words, Word{
			Word:  word,
			Count: count,
		})
		stems[stem].Total += count
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	rank, maxRank := 1, 10000

	for _, stem := range sorted(stems) {
		if rank > maxRank {
			break
		}
		for _, word := range stem.Words {
			fmt.Printf("%s\t%d\t%s\t%d\t%d\n", word.Word, rank, stem.Stem, word.Count, stem.Total)
		}
		rank++
	}
}
