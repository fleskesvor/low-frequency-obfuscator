FAMILIER FÅR ET ENKLERE HVERDAGSLIV I BYEN
Av: Agnete Weisser
Kilde: Adresseavisen, mandag 17/9 2012
Barn i byen
Bylivet gir barna frihet til å utforske omgivelsene på egen hånd. De voksne føler at de får et enklere hverdagsliv. Det forteller forsker og doktorgradsstipendiat Karin Høyland ved Sintef og NTNU. Hun har intervjuet barn, ungdom og foreldre om hvordan der er å bo i byen.
Mindre bilkjøring
På spørsmål om hvorfor det er bra med en by som har rom for barna, sier Høyland: For miljøet er det en fordel at vi bor tettere. Folk som bor i byen bruker mindre bil. Er du vant til å gå eller bruke buss for å komme deg rundt som liten, er sjansene større for at du fortsetter med det som voksen, sier Høyland. Dagens barndom handler mye om organiserte aktiviteter. Både i byens utkanter og på bygda må barna i stor grad kjøres til aktiviteter og venner. Nettopp fordi hverdagen er så organisert, er det viktig med pustehullene, muligheten for barna til å leke fritt og av og til være litt uavhengig av foreldrene. Men skal byen være et godt sted å bo og leve for barn, må perspektivet inn i planleggingen inn fra første stund.
Bedre for alle
Lekeplasser og barnehager i nærmiljøet er viktig. Men en by for barna handler om mye mer. Grønne gårdsrom, fellesareal, tilpassede boliger og gater uten farlig trafikk. Planlegger vi for barna, blir byen et bedre sted for alle, sier Høyland.
Foreldrene mente at hverdagslivet i byen var enklere. De kunne gå mellom boligen og barnehagen, opplevde mindre stress og fikk mer tid sammen med barna. I nærområdet fant de mange kvaliteter. Og de opplevde gårdsrommet som en sosial arena.
Utforsker nærområdet
Nettopp gårdsrommet- eller et bilfritt område mellom blokkene-kan gi barna mulighetene til å utforske på egen hånd. I Trondheim møtte Høyland barn som var veldig stolte over at de gikk til biblioteket på egen hånd. Barna fikk også lov til å gå alene til Solsiden for å kjøpe is. Det syntes de var stas.
Skjermet gårdsrom
Fordi om mange av boligene i byen har skjermet gårdsrom er området rundt bilbasert. På skoleveien er det mye trafikk. Trygge lekeplasser og bilfrie områder betyr mye for barnefamilien. Og selvfølgelig er det viktig med skoler, barnehager og grøntareal i nærheten. Ønsker vi en by for unge enslige, planlegger vi deretter. Men ønsker vi en by for barn, må de være med som et premiss i hele planleggingsprosessen, sier Karin Høyland.
