FROM golang:1.16-alpine3.13 AS builder

COPY . /source
WORKDIR /source
RUN go generate internal/storage/storage.go
RUN go build cmd/server/obfuscate.go

FROM alpine:3.13

RUN apk add --no-cache dumb-init

WORKDIR app
COPY --from=builder /source/obfuscate /app

EXPOSE 8080
ENTRYPOINT ["dumb-init", "--"]
CMD "./obfuscate"
