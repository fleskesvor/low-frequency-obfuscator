package lfo

type Token struct {
	Word string `json:"word"`
	Punctuation string `json:"punctuation"`
	Number bool `json:"number"`
	Name bool `json:"name"`
}

type Entry struct {
	Word  string
	Rank  int
	Stem  string
	Count int
	Total int
}
