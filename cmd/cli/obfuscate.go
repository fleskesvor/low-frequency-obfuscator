package main

import (
	"flag"
	"fmt"
	"gitlab.com/fleskesvor/lfo/internal/obfuscator"
	"gitlab.com/fleskesvor/lfo/internal/storage"
	"io/ioutil"
	"os"
)

func main() {
	var thresholdPtr = flag.Int("t", 0, "threshold for obfuscation (required)")
	var stringPtr = flag.String("s", "", "string to obfuscate (required unless filename is provided)")
	var debugPtr = flag.Bool("d", false, "debug print tokens")

	flag.Usage = usage
	flag.Parse()

	if *thresholdPtr < 1 {
		flag.Usage()
		os.Exit(1)
	}
	if *stringPtr == "" && flag.NArg() == 0 {
		flag.Usage()
		os.Exit(1)
	}
	if flag.Arg(0) != "" && isFile(flag.Arg(0)) {
		if content, err := ioutil.ReadFile(flag.Arg(0)); err == nil {
			*stringPtr = string(content)
		}
	}

	o := obfuscator.NewObfuscator(storage.NewStore())
	text, tokens := o.ObfuscateText(*stringPtr, *thresholdPtr)

	fmt.Println(text)

	if *debugPtr {
		fmt.Printf("\nDEBUG TOKENS:\n")
		for _, token := range tokens {
			fmt.Printf("\t%#v\n", token)
		}
	}
}

func usage() {
	fmt.Printf("Usage: %s -t {-s|filename}\n", os.Args[0])
	flag.PrintDefaults()
}

func isFile(path string) bool {
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
