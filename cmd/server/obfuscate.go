package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/fleskesvor/lfo"
	"gitlab.com/fleskesvor/lfo/internal/obfuscator"
	"gitlab.com/fleskesvor/lfo/internal/storage"
	"log"
	"net/http"
)

type Request struct {
	Text string `json:"text"`
	Threshold int `json:"threshold"`
}

type Response struct {
	Text string `json:"text"`
	Tokens []lfo.Token `json:"tokens"`
}

var o *obfuscator.Obfuscator

func main() {
	o = obfuscator.NewObfuscator(storage.NewStore())
	http.HandleFunc("/", obfuscate)
	fmt.Println("Server started on port 8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func obfuscate(w http.ResponseWriter, r *http.Request) {
	if preflight := handleCors(w, r); preflight {
		return
	}

	var request Request

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	text, tokens := o.ObfuscateText(request.Text, request.Threshold)
	response := Response{
		Text:   text,
		Tokens: tokens,
	}

	if err := json.NewEncoder(w).Encode(response); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func handleCors(w http.ResponseWriter, r *http.Request) bool {
	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", "https://hoppscotch.io")
		w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	}
	return r.Method == http.MethodOptions
}